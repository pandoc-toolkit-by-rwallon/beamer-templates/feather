---
# Color Definitions
color:
    - name: myblue
      type: RGB
      value: 78,103,200
    - name: myorange
      type: RGB
      value: 255,128,33
    - name: mylightblue
      type: RGB
      value: 94,204,243
    - name: mybluegreen
      type: RGB
      value: 93,206,175

# Color Settings
main-color: myblue
title-color: white
header-color: myblue
bar-color: mylightblue
alert-color: myorange
box-color: mybluegreen
---
