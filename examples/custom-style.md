---
# Progress Circle
progress-style: fixed

# Pandoc Integration
bold: true
quote: true
toc: true

# Background Image of the First Slide
background-image: logos/markdown.png
---
