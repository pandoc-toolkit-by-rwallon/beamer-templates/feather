---
# Base Preamble Settings
handout:
language:
package:
    -
header-includes:
    -
include-before:
    -

# Progress Circle
progress-style:
main-logo:

# Color Definitions
color:
    - name:
      type:
      value:

# Color Settings
main-color:
title-color:
header-color:
bar-color:
alert-color:
box-color:

# Font Settings
sans-font:
mono-font:

# Pandoc Integration
bold:
quote:
toc:
links-as-notes:

# Personal Settings
title:
short-title:
subtitle:
author:
    -
short-author:
    -
institute:
    -

# Event Information
event:
date:

# Logos
logo:
    -
logo-height:

# Background Image of the First Slide
background-image:

# Final Highlights
highlights-title:
highlights-width:
highlights-rows:
    - frames:
        -

# Bibliography
biblio-style:
natbiboptions:
    -
bibliography:
    -
---
